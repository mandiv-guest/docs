Storage Server
==============

The storage server is configured via :ref:`ansible`. However, there are manual steps
that we take to configure the RAID array on which the videos are stored.
Typically we have 2 or more large disks in a RAID 1 array (mirrored). Setup follows
something similar to the following::

  # export STORAGE_SERVER=storage # The hostname of the storage server
  # apt install mdadm
  # mdadm --create --verbose /dev/md0 --level=1 --raid-devices=2 /dev/sdb /dev/sdc
  # mkfs.ext4 /dev/md0
  # mkdir /srv/$STORAGE_SERVER
  # echo '/dev/md0 /srv/$STORAGE_SERVER ext4 defaults,nofail,discard 0 0' >> /etc/fstab
  # mount /dev/md0 /srv/$STORAGE_SERVER
  # mdadm --detail --scan >> /etc/mdadm/mdadm.conf
  # update-initramfs -u
