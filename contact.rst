.. _contact us:

Contact us
==========

If you have any questions, you can contact us via:

* our mailing list: debconf-video@lists.debian.org (`archives <https://lists.debian.org/debconf-video/>`_)
* our IRC channel: `#debconf-video`_ on irc.oftc.net

.. _#debconf-video: https://webchat.oftc.net/?nick=visitor&channels=%23debconf-video&prompt=1

